# Stream8-to-AlmaLinux8

Convert Centos Stream8 to AlmaLinux8 for hosts managed with puppet.
Clone the repo and run locally on the host being converted.
```
ansible-playbook ./convert_host.yaml
```

This ansible playbook requires ansible-core to be installed. 
```
dnf remove -y ansible
dnf install -y ansible-core
```

Ansible meta module is part of ansible-core and not ansbile.
